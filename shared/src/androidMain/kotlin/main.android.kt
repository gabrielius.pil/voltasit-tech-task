import android.content.Context
import androidx.compose.runtime.Composable
import di.AppModule

@Composable
fun MainView(darkTheme: Boolean, context: Context) = App(
    darkTheme = darkTheme,
    appModule = AppModule(context = context),
)
