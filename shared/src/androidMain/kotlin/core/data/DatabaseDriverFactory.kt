package core.data

import android.content.Context
import com.myapplication.MyApplication.database.DevicesDatabase
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver

actual class DatabaseDriverFactory(
    private val context: Context
) {
    actual fun create(): SqlDriver {
        return AndroidSqliteDriver(
            DevicesDatabase.Schema,
            context,
            "device.db"
        )
    }
}