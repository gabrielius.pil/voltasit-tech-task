package di

import android.content.Context
import com.myapplication.MyApplication.database.DevicesDatabase
import core.data.DatabaseDriverFactory
import devices.data.KableBluetoothScanner
import devices.data.SqlDelightDeviceDataSource
import devices.domain.BluetoothScanner
import devices.domain.DeviceDataSource

actual class AppModule(
    private val context: Context
) {
    actual val deviceDataSource: DeviceDataSource by lazy {
        SqlDelightDeviceDataSource(
            db = DevicesDatabase(
                driver = DatabaseDriverFactory(context).create()
            )
        )
    }
    actual val bluetoothScanner: BluetoothScanner by lazy {
        KableBluetoothScanner()
    }
}