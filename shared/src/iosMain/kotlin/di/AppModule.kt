package di

import com.myapplication.MyApplication.database.DevicesDatabase
import core.data.DatabaseDriverFactory
import devices.data.KableBluetoothScanner
import devices.data.SqlDelightDeviceDataSource
import devices.domain.BluetoothScanner
import devices.domain.DeviceDataSource

actual class AppModule {
    actual val deviceDataSource: DeviceDataSource by lazy {
        SqlDelightDeviceDataSource(
            db = DevicesDatabase(
                driver = DatabaseDriverFactory().create()
            )
        )
    }
    actual val bluetoothScanner: BluetoothScanner by lazy {
        KableBluetoothScanner()
    }
}