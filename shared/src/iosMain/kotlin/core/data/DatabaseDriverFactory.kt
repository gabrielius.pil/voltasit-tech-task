package core.data

import com.myapplication.MyApplication.database.DevicesDatabase
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.drivers.native.NativeSqliteDriver

actual class DatabaseDriverFactory {
    actual fun create(): SqlDriver {
        return NativeSqliteDriver(
            DevicesDatabase.Schema,
            "device.db"
        )
    }
}