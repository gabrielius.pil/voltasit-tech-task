import androidx.compose.ui.graphics.Color

// Light

val PrimaryLight = Color(0xff006685)
val OnPrimaryLight = Color(0xffffffff)
val PrimaryContainerLight = Color(0xffbfe9ff)
val OnPrimaryContainerLight = Color(0xff001f2a)

val SecondaryLight = Color(0xff4d616c)
val OnSecondaryLight = Color(0xffffffff)
val SecondaryContainerLight = Color(0xffd0e6f2)
val OnSecondaryContainerLight = Color(0xff081e27)

val TertiaryLight = Color(0xff006c4f)
val OnTertiaryLight = Color(0xffffffff)
val TertiaryContainerLight = Color(0xff85f8cb)
val OnTertiaryContainerLight = Color(0xff002116)

val ErrorLight = Color(0xffba1a1a)
val OnErrorLight = Color(0xffffffff)
val ErrorContainerLight = Color(0xffffdad6)
val OnErrorContainerLight = Color(0xff410002)

val BackgroundLight = Color(0xfffbfcfe)
val OnBackgroundLight = Color(0xff191c1e)
val SurfaceLight = Color(0xfffbfcfe)
val OnSurfaceLight = Color(0xff191c1e)

val OutlineLight = Color(0xff70787d)
val SurfaceVariantLight = Color(0xffdce3e9)
val OnSurfaceVariantLight = Color(0xff40484c)


// Dark

val PrimaryDark = Color(0xff6cd2ff)
val OnPrimaryDark = Color(0xff003547)
val PrimaryContainerDark = Color(0xff004d65)
val OnPrimaryContainerDark = Color(0xffbfe9ff)

val SecondaryDark = Color(0xffb4cad6)
val OnSecondaryDark = Color(0xff1f333d)
val SecondaryContainerDark = Color(0xff364954)
val OnSecondaryContainerDark = Color(0xffd0e6f2)

val TertiaryDark = Color(0xff68dbb0)
val OnTertiaryDark = Color(0xff003828)
val TertiaryContainerDark = Color(0xff00513b)
val OnTertiaryContainerDark = Color(0xff85f8cb)

val ErrorDark = Color(0xffffb4ab)
val OnErrorDark = Color(0xff690005)
val ErrorContainerDark = Color(0xff93000a)
val OnErrorContainerDark = Color(0xffffdad6)

val BackgroundDark = Color(0xff191c1e)
val OnBackgroundDark = Color(0xffe1e2e5)
val SurfaceDark = Color(0xff191c1e)
val OnSurfaceDark = Color(0xffe1e2e5)

val OutlineDark = Color(0xff8a9297)
val SurfaceVariantDark = Color(0xff40484c)
val OnSurfaceVariantDark = Color(0xffc0c8cd)