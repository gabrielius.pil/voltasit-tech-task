package core.presentation

import DarkColorScheme
import LightColorScheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable

@Composable
expect fun AppTheme(
    darkTheme: Boolean,
    content: @Composable () -> Unit
)