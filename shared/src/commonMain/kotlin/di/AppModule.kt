package di

import devices.domain.BluetoothScanner
import devices.domain.DeviceDataSource

expect class AppModule {
    val deviceDataSource: DeviceDataSource
    val bluetoothScanner: BluetoothScanner
}