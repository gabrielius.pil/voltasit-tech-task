package devices.domain

import kotlinx.coroutines.flow.Flow

interface DeviceDataSource {
    fun getSavedDevices(): Flow<List<Device>>
    suspend fun insertDevice(device: Device)
    suspend fun updateDevice(device: Device)
    suspend fun deleteDevice(macId: String)
}