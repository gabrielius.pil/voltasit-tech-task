package devices.domain

import kotlinx.coroutines.flow.Flow

interface BluetoothScanner {
    fun scanDevices(): Flow<Device>
}