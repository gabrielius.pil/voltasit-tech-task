package devices.domain

data class Device(
    val macId: String,
    val name: String,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as Device

        if (macId != other.macId) return false
        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        var result = macId.hashCode()
        result = 31 * result + name.hashCode()
        return result
    }
}
