package devices.data

import database.DeviceEntity
import devices.domain.Device


fun DeviceEntity.toDevice(): Device {
    return Device(
        macId = macId,
        name = name,
    )
}