package devices.data

import com.juul.kable.Scanner
import devices.domain.BluetoothScanner
import devices.domain.Device
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class KableBluetoothScanner : BluetoothScanner {
    override fun scanDevices(): Flow<Device> {
        return Scanner()
            .advertisements
            .map {
                Device(
                    macId = it.identifier.toString(),
                    name = it.name ?: "Unknown",
                )
            }
    }
}