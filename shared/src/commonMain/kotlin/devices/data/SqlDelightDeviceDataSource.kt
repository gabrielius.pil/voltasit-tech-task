package devices.data

import com.myapplication.MyApplication.database.DevicesDatabase
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import devices.domain.Device
import devices.domain.DeviceDataSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.datetime.Clock


class SqlDelightDeviceDataSource(
    db: DevicesDatabase
) : DeviceDataSource {

    private val queries = db.deviceQueries

    override fun getSavedDevices(): Flow<List<Device>> {
        return queries
            .getSavedDevices()
            .asFlow()
            .mapToList()
            .map { deviceEntities ->
                deviceEntities.map { deviceEntity ->
                    deviceEntity.toDevice()
                }
            }
    }

    override suspend fun insertDevice(device: Device) {
        queries.insertDeviceEntity(
            macId = device.macId,
            name = device.name,
            createdAt = Clock.System.now().toEpochMilliseconds(),
        )
    }

    override suspend fun updateDevice(device: Device) {
        queries.updateDevice(
            name = device.name,
            macId = device.macId
        )
    }

    override suspend fun deleteDevice(macId: String) {
        queries.deleteDevice(macId)
    }

}