package devices.presentation.composables

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Close
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import core.presentation.SimpleBottomSheet
import devices.domain.Device
import devices.presentation.DevicesEvent
import devices.presentation.DevicesState

@Composable
fun UpdateDevicesNameSheet(
    state: DevicesState,
    name: String?,
    isOpen: Boolean,
    onEvent: (DevicesEvent) -> Unit,
    modifier: Modifier = Modifier,
) {
    SimpleBottomSheet(
        visible = isOpen,
        modifier = modifier
    ) {
        Column {
            Row() {
                IconButton(
                    onClick = {
                        onEvent(DevicesEvent.CloseUpdateDeviceNameSheet)
                    }
                ) {
                    Icon(
                        imageVector = Icons.Rounded.Close,
                        contentDescription = "Close",
                    )
                }
                Spacer(modifier = Modifier.padding(vertical = 12.dp))
                Text(
                    text = "Change Name",
                    fontWeight = FontWeight.Bold,
                    fontSize = 30.sp,
                    modifier = Modifier
                        .padding(PaddingValues(16.dp))
                        .fillMaxWidth()
                )
            }
            Spacer(modifier = Modifier.height(30.dp))
            OutlinedTextField(
                value = name ?: "",
                placeholder = {
                    Text(text = "New name")
                },
                onValueChange = {
                    onEvent(DevicesEvent.OnNameChanged(it))
                },
                shape = RoundedCornerShape(20.dp),
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .fillMaxWidth(),
            )
            Spacer(modifier = Modifier.height(20.dp))
            Text(
                text = "MAC Address: ${state.selectedDevice?.macId}",
                fontWeight = FontWeight.Medium,
                fontSize = 16.sp,
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .fillMaxWidth()
            )

            Spacer(modifier = Modifier.height(30.dp))
            Row(
                horizontalArrangement = Arrangement.Center,
                modifier = Modifier.fillMaxWidth()
            ) {
                Button(
                    onClick = {
                        onEvent(DevicesEvent.UpdateDevice)
                    }
                ) {
                    Text("Update", fontSize = 20.sp)
                }
            }

        }
    }
}