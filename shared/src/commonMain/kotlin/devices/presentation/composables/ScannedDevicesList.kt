package devices.presentation.composables

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Save
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import devices.domain.Device
import devices.presentation.DevicesEvent

@Composable
fun ScannedDevicesList(
    devices: List<Device>,
    onEvent: (DevicesEvent) -> Unit
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 24.dp),
        contentPadding = PaddingValues(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        items(
            devices
        ) { device ->
            DeviceRow(
                device = device,
                isDeviceSaved = false,
                actionIcon = {
                    IconButton(
                        onClick = {
                            onEvent(DevicesEvent.SaveDevice(device))
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Rounded.Save,
                            contentDescription = "Save device",
                        )
                    }
                }
            )
        }
    }
}