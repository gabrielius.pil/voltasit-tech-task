package devices.presentation.composables

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ChevronRight
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import devices.domain.Device
import devices.presentation.DevicesEvent

@Composable
fun SavedItemsList(
    savedDevices: List<Device>,
    onEvent: (DevicesEvent) -> Unit
) {
    Column(
        horizontalAlignment = Alignment.Start,
        modifier = Modifier.padding(PaddingValues(16.dp))
    ) {
        if (savedDevices.isEmpty())
            Text(
                text = "No saved devices yet",
                fontSize = 16.sp,
                fontWeight = FontWeight.Medium,
                modifier = Modifier.padding(top = 20.dp)
            )
        else {
            savedDevices.take(3).forEach {
                DeviceRow(
                    device = it,
                    isDeviceSaved = true,
                    actionIcon = {
                        Row {
                            UpdateIcon(
                                onUpdate = {
                                    onEvent(DevicesEvent.OpenUpdateDeviceNameSheet(device = it))
                                }
                            )
                            Spacer(modifier = Modifier.width(16.dp))
                            DeleteIcon(
                                onDelete = {
                                    onEvent(DevicesEvent.DeleteDevice(it))
                                }
                            )
                        }
                    }
                )
            }
            Row(
                horizontalArrangement = Arrangement.Start,
                modifier = Modifier
                    .fillMaxWidth()
                    .clickable {
                        onEvent(DevicesEvent.OpenAllSavedDevicesSheet)
                    }
                    .padding(PaddingValues(vertical = 16.dp, horizontal = 12.dp))
            ) {
                Icon(
                    imageVector = Icons.Rounded.ChevronRight,
                    contentDescription = "See all"
                )
                Spacer(modifier = Modifier.width(16.dp))
                Text(
                    text = "See all",
                    fontWeight = FontWeight.Medium,
                    fontSize = 18.sp,
                    color = MaterialTheme.colorScheme.tertiary
                )
            }
        }
    }
}