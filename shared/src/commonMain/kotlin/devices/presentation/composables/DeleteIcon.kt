package devices.presentation.composables

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Delete
import androidx.compose.material.icons.rounded.Save
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import devices.presentation.DevicesEvent

@Composable
fun DeleteIcon(
    onDelete: () -> Unit
) {
    IconButton(
        onClick = {
            onDelete()
        }
    ) {
        Icon(
            imageVector = Icons.Rounded.Delete,
            contentDescription = "Delete device",
            tint = MaterialTheme.colorScheme.error
        )
    }
}