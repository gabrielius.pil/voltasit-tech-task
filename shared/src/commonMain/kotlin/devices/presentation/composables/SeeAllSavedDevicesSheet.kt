package devices.presentation.composables

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Close
import androidx.compose.material.icons.rounded.Edit
import androidx.compose.material.icons.rounded.Update
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import core.presentation.SimpleBottomSheet
import devices.presentation.DevicesEvent
import devices.presentation.DevicesState

@Composable
fun SeeAllSavedDevicesSheet(
    state: DevicesState,
    query: String,
    isOpen: Boolean,
    onEvent: (DevicesEvent) -> Unit,
    modifier: Modifier = Modifier,
) {
    SimpleBottomSheet(
        visible = isOpen,
        modifier = modifier
    ) {
        Column {
            Row() {
                IconButton(
                    onClick = {
                        onEvent(DevicesEvent.CloseAllSavedDevicesSheet)
                    }
                ) {
                    Icon(
                        imageVector = Icons.Rounded.Close,
                        contentDescription = "Close",
                    )
                }
                Spacer(modifier = Modifier.padding(vertical = 12.dp))
                Text(
                    text = "Saved Devices",
                    fontWeight = FontWeight.Bold,
                    fontSize = 30.sp,
                    modifier = Modifier
                        .padding(PaddingValues(16.dp))
                        .fillMaxWidth()
                )
            }
            OutlinedTextField(
                value = query,
                placeholder = {
                    Text(text = "Search by Mac Address or Device Name")
                },
                onValueChange = {
                    println("MANO THIS VLAUE $it")
                    println("MANO SELECTED VALUE NAME ${query}")
                    onEvent(DevicesEvent.SearchDeviceByMacIdOrName(it))
                },
                shape = RoundedCornerShape(20.dp),
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .fillMaxWidth(),
            )
            LazyColumn(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(12.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                items(state.savedDevices) { savedDevice ->
                    DeviceRow(
                        device = savedDevice,
                        isDeviceSaved = true,
                        actionIcon = {
                            Row {
                                UpdateIcon(
                                    onUpdate = {
                                        onEvent(DevicesEvent.OpenUpdateDeviceNameSheet(device = savedDevice))
                                    }
                                )
                                Spacer(modifier = Modifier.width(16.dp))
                                DeleteIcon(
                                    onDelete = {
                                        onEvent(DevicesEvent.DeleteDevice(savedDevice))
                                    }
                                )
                            }
                        }
                    )
                }
            }

        }
    }
}