package devices.presentation.composables

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Edit
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable

@Composable
fun UpdateIcon(
    onUpdate: () -> Unit
) {
    IconButton(
        onClick = {
            onUpdate()
        }
    ) {
        Icon(
            imageVector = Icons.Rounded.Edit,
            contentDescription = "Update name",
        )
    }
}