package devices.presentation.composables

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import dev.icerock.moko.mvvm.compose.getViewModel
import dev.icerock.moko.mvvm.compose.viewModelFactory
import dev.icerock.moko.permissions.PermissionsController
import dev.icerock.moko.permissions.compose.BindEffect
import dev.icerock.moko.permissions.compose.PermissionsControllerFactory
import dev.icerock.moko.permissions.compose.rememberPermissionsControllerFactory
import devices.presentation.DeviceViewModel
import di.AppModule

@Composable
fun ScanDevicesScreen(
    appModule: AppModule,
) {
    val factory: PermissionsControllerFactory = rememberPermissionsControllerFactory()
    val permissionController: PermissionsController =
        remember(factory) { factory.createPermissionsController() }

    val viewModel = getViewModel(
        key = "device-screen-view-model",
        factory = viewModelFactory {
            DeviceViewModel(
                permissionController,
                deviceDataSource = appModule.deviceDataSource,
                bluetoothScanner = appModule.bluetoothScanner,
            )
        }
    )
    val devicesState by viewModel.devicesState.collectAsState()
    BindEffect(viewModel.permissionsController)

    Scaffold {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(modifier = Modifier.padding(vertical = 24.dp))
            Text(
                text = "Bluetooth Devices",
                fontWeight = FontWeight.Bold,
                fontSize = 30.sp,
                modifier = Modifier
                    .padding(PaddingValues(16.dp))
                    .fillMaxWidth()
            )
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Start,
                modifier = Modifier.padding(PaddingValues(horizontal = 16.dp)).fillMaxWidth(),
            ) {
                Text(
                    "Saved Devices (${devicesState.savedDevices.size})",
                    color = MaterialTheme.colorScheme.primary,
                )
            }
            SavedItemsList(
                savedDevices = devicesState.savedDevices,
                onEvent = viewModel::onEvent
            )

            Spacer(modifier = Modifier.padding(vertical = 12.dp))
            Text(
                text = "Find New Devices",
                fontWeight = FontWeight.Bold,
                fontSize = 30.sp,
                modifier = Modifier
                    .padding(PaddingValues(16.dp))
                    .fillMaxWidth()
            )
            if (!devicesState.isScanning)
                Box(
                    modifier = Modifier.size(300.dp),
                    contentAlignment = Alignment.Center,
                ) {
                    Button(
                        onClick = {
                            viewModel.onScanPressed()
                        }
                    ) {
                        Text("Scan", fontSize = 20.sp)
                    }
                }
            else
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier.padding(PaddingValues(horizontal = 16.dp)).fillMaxWidth(),
                ) {
                    Text(
                        "Available Devices (${devicesState.devices.size})",
                        color = MaterialTheme.colorScheme.primary
                    )
                    CircularProgressIndicator(
                        modifier = Modifier.size(20.dp),
                        strokeWidth = 3.dp,
                    )
                }
            ScannedDevicesList(
                devices = devicesState.devices,
                onEvent = viewModel::onEvent,
            )
        }
    }
    SeeAllSavedDevicesSheet(
        state = devicesState,
        query = viewModel.searchQuery,
        isOpen = devicesState.isSeeAllDevicesSheetOpen,
        onEvent = viewModel::onEvent
    )

    UpdateDevicesNameSheet(
        state = devicesState,
        name = viewModel.newDeviceName,
        isOpen = devicesState.isUpdateNameSheetOpen,
        onEvent = viewModel::onEvent
    )
}