package devices.presentation

import devices.domain.Device

data class DevicesState(
    val devices: List<Device> = emptyList(),
    val savedDevices: List<Device> = emptyList(),
    val isScanning: Boolean = false,
    val isSeeAllDevicesSheetOpen: Boolean = false,
    val isUpdateNameSheetOpen: Boolean = false,
    val selectedDevice: Device? = null,
)
