package devices.presentation

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.text.toLowerCase
import dev.icerock.moko.mvvm.viewmodel.ViewModel
import dev.icerock.moko.permissions.DeniedAlwaysException
import dev.icerock.moko.permissions.DeniedException
import dev.icerock.moko.permissions.Permission
import dev.icerock.moko.permissions.PermissionsController
import devices.domain.BluetoothScanner
import devices.domain.Device
import devices.domain.DeviceDataSource
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class DeviceViewModel(
    val permissionsController: PermissionsController,
    private val deviceDataSource: DeviceDataSource,
    private val bluetoothScanner: BluetoothScanner,
) : ViewModel() {

    private val _devices = mutableSetOf<Device>()

    private val _deviceState = MutableStateFlow(DevicesState(_devices.toList()))

    var searchQuery by mutableStateOf("")
        private set

    var newDeviceName: String? by mutableStateOf(null)
        private set

    val devicesState: StateFlow<DevicesState> = combine(
        _deviceState, deviceDataSource.getSavedDevices(), snapshotFlow { searchQuery }
    ) { devicesState, savedDevicesList, searchQuery ->
        devicesState.copy(
            devices = _devices.toList(),
            savedDevices = if (searchQuery.isBlank())
                savedDevicesList
            else
                savedDevicesList.filter { device ->
                    device.macId.lowercase().startsWith(searchQuery.lowercase()) ||
                            device.name.lowercase().startsWith(searchQuery.lowercase())
                },
        )
    }.stateIn(
        viewModelScope, SharingStarted.WhileSubscribed(5000L), DevicesState(_devices.toList())
    )

    fun onScanPressed() {
        viewModelScope.launch {
            try {
                permissionsController.providePermission(Permission.BLUETOOTH_SCAN)
                permissionsController.providePermission(Permission.BLUETOOTH_CONNECT)
                scanDevices()
            } catch (deniedAlways: DeniedAlwaysException) {
                // Permission denied always
            } catch (denied: DeniedException) {
                // Permission denied
            }
        }
    }

    private suspend fun scanDevices() {
        bluetoothScanner.scanDevices().collectLatest { scannedDevice ->
            _devices.add(scannedDevice)
            _deviceState.update {
                it.copy(
                    devices = _devices.toList(), isScanning = true
                )
            }
        }
    }

    fun onEvent(event: DevicesEvent) {
        when (event) {
            is DevicesEvent.SaveDevice -> {
                viewModelScope.launch {
                    deviceDataSource.insertDevice(event.device)
                }
            }

            is DevicesEvent.UpdateDevice -> {
                viewModelScope.launch {
                    deviceDataSource.updateDevice(
                        Device(
                            macId = devicesState.value.selectedDevice?.macId ?: "",
                            name = newDeviceName ?: ""
                        )
                    )
                    _deviceState.update {
                        it.copy(
                            isUpdateNameSheetOpen = false,
                            selectedDevice = null,
                        )
                    }
                    delay(300L) // Animation delay
                    newDeviceName = null
                }
            }

            is DevicesEvent.DeleteDevice -> {
                viewModelScope.launch {
                    deviceDataSource.deleteDevice(event.device.macId)
                }
            }

            DevicesEvent.CloseAllSavedDevicesSheet -> {
                viewModelScope.launch {
                    _deviceState.update {
                        it.copy(
                            isSeeAllDevicesSheetOpen = false
                        )
                    }
                    searchQuery = ""
                }
            }

            DevicesEvent.OpenAllSavedDevicesSheet -> {
                viewModelScope.launch {
                    _deviceState.update {
                        it.copy(
                            isSeeAllDevicesSheetOpen = true
                        )
                    }
                }
            }

            is DevicesEvent.SearchDeviceByMacIdOrName -> {
                searchQuery = event.query
            }

            DevicesEvent.CloseUpdateDeviceNameSheet -> {
                newDeviceName = null
                viewModelScope.launch {
                    _deviceState.update {
                        it.copy(
                            isUpdateNameSheetOpen = false,
                            selectedDevice = null,
                        )
                    }
                }
            }

            is DevicesEvent.OpenUpdateDeviceNameSheet -> {
                newDeviceName = event.device.name
                viewModelScope.launch {
                    _deviceState.update {
                        it.copy(
                            isUpdateNameSheetOpen = true,
                            selectedDevice = event.device
                        )
                    }
                }
            }

            is DevicesEvent.OnNameChanged -> {
                newDeviceName = event.name
            }
        }
    }
}