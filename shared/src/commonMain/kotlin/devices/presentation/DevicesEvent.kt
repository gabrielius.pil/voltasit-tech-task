package devices.presentation

import devices.domain.Device

sealed interface DevicesEvent {
    data class SaveDevice(val device: Device) : DevicesEvent
    data class DeleteDevice(val device: Device) : DevicesEvent

    data class OnNameChanged(val name: String) : DevicesEvent
    data object UpdateDevice : DevicesEvent
    data class OpenUpdateDeviceNameSheet(val device: Device) : DevicesEvent
    data object CloseUpdateDeviceNameSheet : DevicesEvent

    data object OpenAllSavedDevicesSheet : DevicesEvent
    data object CloseAllSavedDevicesSheet : DevicesEvent

    data class SearchDeviceByMacIdOrName(val query: String) : DevicesEvent
}