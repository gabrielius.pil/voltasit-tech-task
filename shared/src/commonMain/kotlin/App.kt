import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import core.presentation.AppTheme
import devices.presentation.composables.ScanDevicesScreen
import di.AppModule

@Composable
fun App(
    darkTheme: Boolean,
    appModule: AppModule,
) {
    AppTheme(
        darkTheme = darkTheme
    ) {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background,
        ) {
            ScanDevicesScreen(appModule = appModule)
        }
    }
}


